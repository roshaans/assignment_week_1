const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');

const app = express();
const port = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({ extended: true }));


let users = {};
try {
    users = JSON.parse(fs.readFileSync('data.txt', 'utf8'));
} catch (error) {
    console.error('Error loading users:', error);
}

app.get('/', (req, res) => {
    res.send(`
        <h1>Login Form</h1>
        <form action="/login" method="post">
            <label for="username">Username:</label><br>
            <input type="text" id="username" name="username"><br>
            <label for="password">Password:</label><br>
            <input type="password" id="password" name="password"><br><br>
            <input type="submit" value="Login">
        </form>
    `);
});

app.post('/login', (req, res) => {
    const { username, password } = req.body;
    if (username && password) {
        if (users[username] && users[username] === password) {
            res.send('Login successful');
        } else {
            res.status(401).send('Invalid username or password');
        }
    } else {
        res.status(400).send('Username and password are required');
    }
});

app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
